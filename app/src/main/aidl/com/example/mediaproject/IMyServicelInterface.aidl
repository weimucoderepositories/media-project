// IMyServicelInterface.aidl
package com.example.mediaproject;

// Declare any non-default types here with import statements

interface IMyServicelInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);

     void setPositon(int positon);

     void start();

      void pause();

      void next();

      void previous();

      int getPosition();

      int getCurrentTime();

      int getTotalTime();

      String getSongName();

      String getSongCreater();

      void seekTime(int toTime);
}