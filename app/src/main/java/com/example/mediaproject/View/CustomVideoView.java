package com.example.mediaproject.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.VideoView;

public class CustomVideoView extends VideoView {
    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
       setMeasuredDimension(widthMeasureSpec,heightMeasureSpec);
    }
    //设置测量值  设置layout的值 之后 就会调用 onMeasure
    public void setVideoViewSize(int width, int height){
        ViewGroup.LayoutParams params = getLayoutParams();
        params.height = height;
        params.width = width;
        setLayoutParams(params);
    }
}
