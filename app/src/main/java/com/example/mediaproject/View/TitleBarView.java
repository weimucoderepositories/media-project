package com.example.mediaproject.View;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.mediaproject.R;

public class TitleBarView extends LinearLayout implements View.OnClickListener {

    private View searchView;
    private View gameView;
    private View historyView;
    private Context context;
    public TitleBarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        searchView = this.findViewById(R.id.titlebar_search_id);
        gameView = this.findViewById(R.id.titlebar_game_id);
        historyView = this.findViewById(R.id.titlebar_history_id);

        searchView.setOnClickListener(this);
        gameView.setOnClickListener(this);
        historyView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.titlebar_search_id:
                Toast.makeText(context,"search",Toast.LENGTH_SHORT).show();
                break;
            case R.id.titlebar_game_id:
                Toast.makeText(context,"game",Toast.LENGTH_SHORT).show();

                break;
            case R.id.titlebar_history_id:
                Toast.makeText(context,"history",Toast.LENGTH_SHORT).show();

                break;
        }
    }
}
