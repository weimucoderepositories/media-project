package com.example.mediaproject.Base;

import android.content.Context;
import android.view.View;

public abstract class BasePager {
    public Context context;
    public View rootView;
    public Boolean isInitView = false;

    public BasePager(Context context){
        this.context = context;
        rootView = initView();
    }
    //子类必须实现的静态方法 方法为静态 类也必须是静态
    public abstract View initView();
    //用于子类 初始化 数据
    public void  initData(){

    }
}
