package com.example.mediaproject.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mediaproject.Activity.SystemVideoPayerActivity;
import com.example.mediaproject.Base.BasePager;
import com.example.mediaproject.Beans.MediaItem;
import com.example.mediaproject.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.ArrayList;

public class NetVedioPager extends BasePager {

    private static final String TAG = "NetVedioPager";

    private ListView listView;
    private View notDataView;
    private ArrayList<MediaItem> mediaItems = new ArrayList<>();
    private MyAdapter adapter;
    public NetVedioPager(Context context) {
        super(context);
    }

    @Override
    public View initView() {
        View view  = LayoutInflater.from(context).inflate(R.layout.net_videopager,null);
        listView = view.findViewById(R.id.net_video_listview);
        notDataView = view.findViewById(R.id.netnot_datas_view);

        SmartRefreshLayout smartRefreshLayout =  view.findViewById(R.id.refreshlayout);
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2000/*,false*/);//传入false表示刷新失败
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                refreshlayout.finishLoadMore(2000/*,false*/);//传入false表示加载失败
            }
        });
        adapter = new MyAdapter();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(context, SystemVideoPayerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("MediaItems",mediaItems);
                intent.putExtras(bundle);
                intent.putExtra("position",i);
                context.startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void initData() {
        super.initData();
        getDatas();
    }

    private void getDatas(){

        RequestParams params = new RequestParams("http://c.3g.163.com/nc/video/list/VAP4BFR16/y/0-10.html");
        x.http().get(params, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Log.d(TAG, "onSuccess: " + result);
                parseJosn(result);
                if (mediaItems != null && mediaItems.size()>0){
                    notDataView.setVisibility(View.GONE);
                }else {
                    notDataView.setVisibility(View.VISIBLE);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Log.d(TAG, "onError: " + ex.toString());
            }

            @Override
            public void onCancelled(CancelledException cex) {
                Log.d(TAG, "onCancelled: ");
            }

            @Override
            public void onFinished() {
                Log.d(TAG, "onFinished: ");
            }
        });


    }
     public void parseJosn(String jsonStr){

        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray jsonArray = jsonObject.optJSONArray("VAP4BFR16");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = (JSONObject)jsonArray.get(i);
                MediaItem mediaItem = new MediaItem();
                String title = object.optString("title");
                String subTitle = object.optString("topicDesc");
                String filePath = object.optString("mp4_url");
                String imageStr = object.optString("cover");
                String size = object.optString("sizeSD");
                String m3u8Str = object.optString("m3u8_url");

                mediaItem.setName(title);
                mediaItem.setSubTitle(subTitle);
                mediaItem.setFilePath(filePath);
                mediaItem.setConverImg(imageStr);
                mediaItem.setSize(size);
                mediaItem.setM3u8_url(m3u8Str);
                mediaItems.add(mediaItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            if (mediaItems != null){
                return mediaItems.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int i) {
            if (mediaItems != null){
                return mediaItems.get(i);
            }
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHodler viewHolder = null;
            if (view == null){
                view = LayoutInflater.from(context).inflate(R.layout.net_video_listitem,viewGroup,false);
                viewHolder = new ViewHodler();
                viewHolder.iconImage = view.findViewById(R.id.net_video_icon);
                viewHolder.titleTV = view.findViewById(R.id.net_video_name_tv);
                viewHolder.subTitleTV = view.findViewById(R.id.netvideo_sutitle_tv);
                viewHolder.sizeTV = view.findViewById(R.id.netvideo_size_tv);
                view.setTag(viewHolder);
            }else {
                viewHolder = (ViewHodler)view.getTag();
            }

            MediaItem mediaItem = (MediaItem)getItem(i);

            //加载图片
//            x.image().bind(viewHolder.iconImage,mediaItem.getConverImg());
//Glide加载图片
            Glide.with(context).
                    load(mediaItem.getConverImg()).
                    placeholder(R.drawable.bg_logo).
                    into(viewHolder.iconImage);

            viewHolder.titleTV.setText(mediaItem.getName() == null ? "" : mediaItem.getName());
            viewHolder.subTitleTV.setText(mediaItem.getSubTitle());
            viewHolder.sizeTV.setText(mediaItem.getSize());
            return view;
        }
    }
    class ViewHodler{
        public ImageView iconImage;
        public TextView titleTV;
        public TextView subTitleTV;
        public TextView sizeTV;
    }

}
