package com.example.mediaproject.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.mediaproject.Activity.AudioPlayerActivity;
import com.example.mediaproject.Activity.SystemVideoPayerActivity;
import com.example.mediaproject.Base.BasePager;
import com.example.mediaproject.Beans.MediaItem;
import com.example.mediaproject.R;
import com.example.mediaproject.Utils.Util;

import java.util.ArrayList;



public class LocAudioPager extends BasePager {

    private ListView listView;
    private View notDataView;
    private ArrayList<MediaItem> mediaItems = new ArrayList<>();
    private ArrayList<MediaItem> tempMediaItems = new ArrayList<>();
    private LocAudioPager.MyAdapter myAdapter;

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message message) {
            if (tempMediaItems != null && tempMediaItems.size() > 0){
                notDataView.setVisibility(View.GONE);
                mediaItems.clear();
                mediaItems.addAll(tempMediaItems);
                myAdapter.notifyDataSetChanged();

            }else {
                notDataView.setVisibility(View.VISIBLE);
            }

            return false;
        }
    });



    public LocAudioPager(Context context) {
        super(context);
    }

    @Override
    public View initView() {

        View view = LayoutInflater.from(context).inflate(R.layout.loc_audio_pager,null,false);
        listView = view.findViewById(R.id.loc_audio_listview);
        notDataView = view.findViewById(R.id.not_datas_view);

        myAdapter = new LocAudioPager.MyAdapter();
        listView.setAdapter(myAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MediaItem item = mediaItems.get(i);

//                Intent intent = new Intent(context, SystemVideoPayerActivity.class);
//                File file = new File(item.getFilePath());
//                intent.setDataAndType(Uri.fromFile(file),"video/*");
//                context.startActivity(intent);

                Intent intent = new Intent(context, AudioPlayerActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtra("position",i);
                context.startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void initData() {
        super.initData();
        getDatas();
    }


    @SuppressLint("Range")
    private void getDatas(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                String permiss[] = {Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (ContextCompat.checkSelfPermission(context,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions((Activity) context,permiss ,10029);
                }else {
                    ContentResolver resolver = context.getContentResolver();
                    Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    String[] params = {
                            MediaStore.Audio.Media.DISPLAY_NAME, //在sd卡显示的名称
                            MediaStore.Audio.Media.DURATION,  //视频长度
                            MediaStore.Audio.Media.SIZE,  //视频大小
                            MediaStore.Audio.Media.DATA  //视频绝对路径
                    };
                    Cursor cursor = resolver.query(uri,params,null,null,null);
                    if (cursor != null){
                        while (cursor.moveToNext()){

                            String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
                            String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                            String size = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.SIZE));
                            String filePath = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                            MediaItem item = new MediaItem();
                            item.setName(name);
                            item.setDuration(duration);
                            item.setSize(size);
                            item.setFilePath(filePath);
                            tempMediaItems.add(item);
                        }
                    }
                    handler.sendEmptyMessage(100000000);

                }
            }
        }).start();


    }

   private  class MyAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            if (mediaItems != null){
                return mediaItems.size();
            }else {
                return 0;
            }

        }

        @Override
        public Object getItem(int i) {
            if (mediaItems != null) {
                return mediaItems.get(i);
            }else{
                return null;
            }
        }

        @Override
        public long getItemId(int i) {
            return i;
        }


        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null){
                view = LayoutInflater.from(context).inflate(R.layout.loc_audio_listitem,viewGroup,false);
                viewHolder = new ViewHolder();
                viewHolder.nameTV = view.findViewById(R.id.loc_audio_name_tv);
                viewHolder.durationTV = view.findViewById(R.id.loc_audio_duration_tv);
                viewHolder.sizeTV = view.findViewById(R.id.loc_audio_size_tv);
                viewHolder.iconImg = view.findViewById(R.id.loc_audio_icon);
                view.setTag(viewHolder);
            }else{
                viewHolder =  (ViewHolder)view.getTag();
            }
            MediaItem mediaItem = (MediaItem)getItem(i);
            viewHolder.nameTV.setText(mediaItem.getName() == null ? "wu" : mediaItem.getName());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && mediaItem.getDuration() != null) {
                viewHolder.durationTV.setText(Util.stringForTime(Integer.parseInt(mediaItem.getDuration())));
            }else {
                viewHolder.durationTV.setText("1:20:10");
            }
            viewHolder.sizeTV.setText(Formatter.formatFileSize(context,Long.parseLong(mediaItem.getSize())) );
            return view;
        }

        class ViewHolder{
            private TextView nameTV;
            private TextView durationTV;
            private TextView sizeTV;
            private ImageView iconImg;
        }
    }

}

