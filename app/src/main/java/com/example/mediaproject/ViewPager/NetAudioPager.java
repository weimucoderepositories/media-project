package com.example.mediaproject.ViewPager;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.example.mediaproject.Base.BasePager;

public class NetAudioPager extends BasePager {

    private TextView textView;
    public NetAudioPager(Context context) {
        super(context);
    }

    @Override
    public View initView() {
         textView = new TextView(context);

        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(12);
        return textView;
    }

    @Override
    public void initData() {
        super.initData();
        textView.setText("我是网络音乐");
    }
}
