package com.example.mediaproject.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.mediaproject.Beans.MediaItem;
import com.example.mediaproject.R;
import com.example.mediaproject.Utils.Util;
import com.example.mediaproject.View.CustomVideoView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SystemVideoPayerActivity extends AppCompatActivity implements View.OnClickListener {

    private CustomVideoView videoView;

    private LinearLayout videoviewStatusbar;
    private TextView videoviewName;
    private ImageView videoviewBattery;
    private TextView videoviewSystemtime;
    private LinearLayout videoviewVoicestatus;
    private ImageButton videoviewVoice;
    private SeekBar videoviewVoicepress;
    private ImageButton videoviewPlayerSwitch;
    private TextView videoviewCurrenttime;
    private SeekBar videoviewSeekbar;
    private TextView videoviewTotaltime;
    private LinearLayout videoviewPlayercontrol;
    private ImageButton videoviewBack;
    private ImageButton videoviewPre;
    private ImageButton videoviewPlay;
    private ImageButton videoviewNext;
    private ImageButton videoviewSwitchsreen;

    private MyBroadCastReceiver receiver;
    private static final int PROGRESS = 1000;
    private ArrayList<MediaItem> mediaItems;
    private int position;
    private int screenWidth;
    private int screenHeight;

    private int mVideoWidth = 0;
    private int mVideoHeight = 0;

    private int currentVolume;
    private int maxVolume;
    private AudioManager audioManager;
    //设置手势监听
    private GestureDetector gestureDetector;

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(@NonNull Message message) {

            int what = message.what;
            switch (what){
                case PROGRESS:{

                    videoviewSeekbar.setMax(videoView.getDuration());
                    int currentPosition = videoView.getCurrentPosition();
                    videoviewSeekbar.setProgress(currentPosition);
                    videoviewCurrenttime.setText(currentPosition+"");
                    videoviewSystemtime.setText(getSystemTime());
                    handler.removeMessages(PROGRESS);
                    Message temp = new Message();
                    temp.what = PROGRESS;
                    handler.sendMessageDelayed(temp,1000);
                }
                break;
            }

            return false;
        }
    });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_video_payer);
        videoView = findViewById(R.id.video_view_id);

        initDatas();
        findViews();
        setOnlistenser();
        getDatas();
        //设置控制面板
//        videoView.setMediaController(new MediaController(SystemVideoPayerActivity.this));


    }

    private void getDatas() {
        mediaItems =  (ArrayList<MediaItem>)getIntent().getSerializableExtra("MediaItems");
        position = getIntent().getIntExtra("position",0);

        Uri uri = getIntent().getData();


        if (mediaItems != null && mediaItems.size() > 0){
            MediaItem item = mediaItems.get(position);
            videoView.setVideoPath(item.getFilePath());
        }else if(uri != null){
            videoView.setVideoURI(uri);
        }
        videoView.setKeepScreenOn(true);
    }

    private void setOnlistenser(){

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.start();

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        //拖动结束
                    }
                });

                int duration = videoView.getDuration();

                videoviewTotaltime.setText( duration+"");

                handler.sendEmptyMessage(PROGRESS);
                mVideoWidth = mediaPlayer.getVideoWidth();
                mVideoHeight = mediaPlayer.getVideoHeight();
//                videoView.setVideoViewSize(300,200);
                //设置缓冲信息
                int buffer = videoView.getBufferPercentage();
                videoviewSeekbar.setSecondaryProgress(buffer);

            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                Toast.makeText(SystemVideoPayerActivity.this,"播放失败",Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Toast.makeText(SystemVideoPayerActivity.this,"播放完成",Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        videoviewSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b){
                    videoView.seekTo(i);
                }
            }

            //当手指碰触
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            //当手指结束
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        videoviewVoicepress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b){

                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,i,0);
                    currentVolume = i;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        videoView.setOnInfoListener(new MyOninfoListener());
    }

    class MyOninfoListener implements MediaPlayer.OnInfoListener{

        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
           //网络卡开始
           if (what == MediaPlayer.MEDIA_INFO_BUFFERING_START){

           }else if(what == MediaPlayer.MEDIA_INFO_BUFFERING_END){//网络卡结束

           }

            return false;
        }
    }
    private void findViews(){
        videoviewStatusbar = (LinearLayout) findViewById(R.id.videoview_statusbar_id);
        videoviewName = (TextView) findViewById(R.id.videoview_name_id);
        videoviewBattery = (ImageView) findViewById(R.id.videoview_battery_id);
        videoviewSystemtime = (TextView) findViewById(R.id.videoview_systemtime_id);
        videoviewVoicestatus = (LinearLayout) findViewById(R.id.videoview_voicestatus_id);
        videoviewVoice = (ImageButton) findViewById(R.id.videoview_voice_id);
        videoviewVoicepress = (SeekBar) findViewById(R.id.videoview_voicepress_id);
        videoviewPlayerSwitch = (ImageButton) findViewById(R.id.videoview_player_switch);
        videoviewCurrenttime = (TextView) findViewById(R.id.videoview_currenttime_id);
        videoviewSeekbar = (SeekBar) findViewById(R.id.videoview_seekbar_id);
        videoviewTotaltime = (TextView) findViewById(R.id.videoview_totaltime_id);
        videoviewPlayercontrol = (LinearLayout) findViewById(R.id.videoview_playercontrol_id);
        videoviewBack = (ImageButton) findViewById(R.id.videoview_back_id);
        videoviewPre = (ImageButton) findViewById(R.id.videoview_pre_id);
        videoviewPlay = (ImageButton) findViewById(R.id.videoview_play_id);
        videoviewNext = (ImageButton) findViewById(R.id.videoview_next_id);
        videoviewSwitchsreen = (ImageButton) findViewById(R.id.videoview_switchsreen_id);


        videoviewBack.setOnClickListener(this);
        videoviewPre.setOnClickListener(this);
        videoviewPlay.setOnClickListener(this);
        videoviewNext.setOnClickListener(this);
        videoviewSwitchsreen.setOnClickListener(this);
        videoviewVoicepress.setMax(maxVolume);
        videoviewSeekbar.setProgress(currentVolume);

    }

    private void initDatas(){

        //获取屏幕的宽高
        WindowManager windowManager = getWindowManager();
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        //注册监听
         receiver = new MyBroadCastReceiver();
        registerReceiver(receiver,intentFilter);
        //实例化 手势监听器
        gestureDetector =  new GestureDetector(this,new CustomGestureDetector());

         audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
         currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
         maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

    }

    private boolean isFullScreen = false;
   //添加监听器
    class CustomGestureDetector extends GestureDetector.SimpleOnGestureListener {

      //单击
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Toast.makeText(SystemVideoPayerActivity.this,"onSingleTapConfirmed",Toast.LENGTH_SHORT).show();
            return super.onSingleTapConfirmed(e);
        }

        //双击击
        @Override
        public boolean onDoubleTap(MotionEvent e) {
             if (isFullScreen){
                isFullScreen = false;
                int width = screenWidth;
                int height = screenHeight;
                if (mVideoWidth > 0 && mVideoHeight > 0){
                    if (mVideoWidth * height < width * mVideoHeight){
                        width = height * mVideoWidth / mVideoHeight;
                    }else if (mVideoWidth * height > width *mVideoHeight){
                        height = width * mVideoHeight / mVideoWidth;
                    }
                }

                videoView.setVideoViewSize(width,height);
            }else {
                isFullScreen = true;
                videoView.setVideoViewSize(screenWidth,screenHeight);
            }


            return super.onDoubleTap(e);
        }


        //长按
        @Override
        public void onLongPress(MotionEvent motionEvent) {
            Toast.makeText(SystemVideoPayerActivity.this,"onLongPress",Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.videoview_back_id:
                break;
            case R.id.videoview_pre_id:

                if (mediaItems != null && mediaItems.size() > 0 &&   position > 0){
                    position--;
                    MediaItem item = mediaItems.get(position);
                    videoView.setVideoPath(item.getFilePath());
                }else {
                    MediaItem item = mediaItems.get(0);
                    videoView.setVideoPath(item.getFilePath());
                }
                break;
            case R.id.videoview_play_id:
                if (videoView.isPlaying()){
                    videoView.pause();
                    videoviewPlay.setBackgroundResource(R.drawable.btn_play_normal);
                }else {
                    videoView.start();
                    videoviewPlay.setBackgroundResource(R.drawable.btn_pause_normal);
                }
                break;
            case R.id.videoview_next_id:
                if (mediaItems != null && mediaItems.size() > 0 && mediaItems.size() > position){
                    position++;
                    MediaItem item = mediaItems.get(position);
                    videoView.setVideoPath(item.getFilePath());
                }

                break;
            case R.id.videoview_switchsreen_id:
                break;
        }
    }

    class MyBroadCastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
               int level =  intent.getIntExtra("level",0);
               if (level >= 0 && level <  10){
                   videoviewBattery.setImageResource(R.drawable.ic_battery_0);
               }else  if (level >= 10 && level <  20){
                   videoviewBattery.setImageResource(R.drawable.ic_battery_20);
               }

               else  if (level >= 20 && level <  40){
                   videoviewBattery.setImageResource(R.drawable.ic_battery_40);
               }

               else  if (level >= 50 && level <  60){
                   videoviewBattery.setBackgroundResource(R.drawable.ic_battery_60);
               } else  if (level >= 60 && level <  80){
                   videoviewBattery.setImageResource(R.drawable.ic_battery_80);
               }else {
                   videoviewBattery.setImageResource(R.drawable.ic_battery_100);
               }
        }
    }

    private String getSystemTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        return dateFormat.format(new Date());
    }

    @Override
    protected void onDestroy() {

        if (receiver != null){
            unregisterReceiver(receiver);
        }
        super.onDestroy();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //把事件赋值给gesture
        gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    //物理键盘 监听 接管
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //监听音量的变化
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){

        }else if(keyCode == KeyEvent.KEYCODE_VOLUME_UP){

        }
        return super.onKeyDown(keyCode, event);
    }
}