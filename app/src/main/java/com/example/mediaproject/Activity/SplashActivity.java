package com.example.mediaproject.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import com.example.mediaproject.R;

public class SplashActivity extends AppCompatActivity {

    private boolean isEnter = false;
    private  Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //这是主线程
                startMainActivity();
            }


        },1 * 1000);
    }

    private void startMainActivity() {
//        if (!isEnter){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            isEnter = true;
            //移出掉
            finish();

//        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //会执行touchdown touchup等方法  多次执行 MainActivity 设置为singleTask 内存中只有一份
        startMainActivity();
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //移出消息
        handler.removeCallbacksAndMessages(null);
    }
}