package com.example.mediaproject.Activity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.mediaproject.Base.BasePager;
import com.example.mediaproject.Fragment.MyPagerFragment;
import com.example.mediaproject.R;
import com.example.mediaproject.ViewPager.LocAudioPager;
import com.example.mediaproject.ViewPager.LocVideoPager;
import com.example.mediaproject.ViewPager.NetAudioPager;
import com.example.mediaproject.ViewPager.NetVedioPager;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private List<BasePager> pagerList;
    public int positon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = findViewById(R.id.radio_group_id);
        pagerList = new ArrayList<>();

        LocVideoPager videoPager = new LocVideoPager(this);
        LocAudioPager audioPager = new LocAudioPager(this);
        NetVedioPager netVedioPager = new NetVedioPager(this);
        NetAudioPager netAudioPager = new NetAudioPager(this);
        pagerList.add(videoPager);
        pagerList.add(audioPager);
        pagerList.add(netVedioPager);
        pagerList.add(netAudioPager);


       radioGroup.setOnCheckedChangeListener(new MyCheckedChangeListener());
        ((RadioButton)findViewById(R.id.tab_video_loc)).setChecked(true);
    }

    private class MyCheckedChangeListener implements RadioGroup.OnCheckedChangeListener{

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i){
                case R.id.tab_video_loc:
                    positon = 0;
                    break;
                case R.id.tab_audio_loc:
                    positon = 1;
                    break;
                case R.id.tab_video_net:
                    positon = 2;
                    break;
                case R.id.tab_audio_net:
                    positon = 3;
                    break;
            }
            setFragement();
        }
    }

    public void setFragement() {
      FragmentManager fm = getSupportFragmentManager();
      FragmentTransaction ft = fm.beginTransaction();

      MyPagerFragment pagerFragment = new MyPagerFragment(pagerList,positon);
      ft.replace(R.id.content_framelayout,pagerFragment);
      ft.commit();
    }

}



