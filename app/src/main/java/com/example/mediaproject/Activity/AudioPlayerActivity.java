package com.example.mediaproject.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.mediaproject.IMyServicelInterface;
import com.example.mediaproject.R;
import com.example.mediaproject.Service.MediaService;

import java.security.Provider;

public class AudioPlayerActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView audioTitleName;
    private TextView audioActorName;
    private TextView audioTimeName;
    private SeekBar audioviewSeekbar;
    private LinearLayout audioviewPlayercontrol;
    private ImageButton audioviewBack;
    private ImageButton audioviewPre;
    private ImageButton audioviewPlay;
    private ImageButton audioviewNext;
    private ImageButton audioviewSwitchsreen;
    private int positon;

    IMyServicelInterface servicelInterface = null;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            servicelInterface = IMyServicelInterface.Stub.asInterface(service);
            try {
                servicelInterface.setPositon(0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_player);

        findViews();
        onListener();

        positon = getIntent().getIntExtra("position",0);
        Intent intent1 = new Intent(this, MediaService.class);
        intent1.setAction("cn.com.mediaplayer.OpenAudio");
        bindService(intent1,connection, Context.BIND_AUTO_CREATE);
        startService(intent1);
    }

    private void findViews(){

        imageView = findViewById(R.id.audio_image_animation);
        imageView.setBackgroundResource(R.drawable.frame_animation);
        AnimationDrawable drawable = (AnimationDrawable)imageView.getBackground();
        drawable.start();
        audioTitleName = (TextView) findViewById(R.id.audio_title_name);
        audioActorName = (TextView) findViewById(R.id.audio_actor_name);
        audioTimeName = (TextView) findViewById(R.id.audio_time_name);
        audioviewSeekbar = (SeekBar) findViewById(R.id.audioview_seekbar_id);
        audioviewPlayercontrol = (LinearLayout) findViewById(R.id.audioview_playercontrol_id);
        audioviewBack = (ImageButton) findViewById(R.id.audioview_back_id);
        audioviewPre = (ImageButton) findViewById(R.id.audioview_pre_id);
        audioviewPlay = (ImageButton) findViewById(R.id.audioview_play_id);
        audioviewNext = (ImageButton) findViewById(R.id.audioview_next_id);
        audioviewSwitchsreen = (ImageButton) findViewById(R.id.audioview_switchsreen_id);

    }

    private void onListener(){
        audioviewSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        audioviewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        audioviewPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        audioviewPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        audioviewPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        audioviewNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        audioviewSwitchsreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connection!= null){//并没有解绑service
            unbindService(connection);
        }
    }
}