package com.example.mediaproject.Utils;

import android.icu.text.SimpleDateFormat;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;


public class Util {

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String stringForTime(int tieMs){

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.CHINA);
       String time = dateFormat.format(new Date(tieMs));
       return time;
    }
}
