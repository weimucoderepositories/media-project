package com.example.mediaproject.Beans;

import java.util.Map;

public class NetMediaItem {
    private Map<String,String> videoTopic;

    private String title;
    private String topicDesc;
    private String mp4_url;
    private String cover;
    private String sizeSD;

    public Map<String, String> getVideoTopic() {
        return videoTopic;
    }

    public void setVideoTopic(Map<String, String> videoTopic) {
        this.videoTopic = videoTopic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTopicDesc() {
        return topicDesc;
    }

    public void setTopicDesc(String topicDesc) {
        this.topicDesc = topicDesc;
    }

    public String getMp4_url() {
        return mp4_url;
    }

    public void setMp4_url(String mp4_url) {
        this.mp4_url = mp4_url;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getSizeSD() {
        return sizeSD;
    }

    public void setSizeSD(String sizeSD) {
        this.sizeSD = sizeSD;
    }
}
