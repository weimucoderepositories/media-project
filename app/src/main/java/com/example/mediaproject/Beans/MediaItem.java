package com.example.mediaproject.Beans;

import java.io.Serializable;

public class MediaItem implements Serializable {

    private String name;
    private String duration;
    private String size;
    private String filePath;
     private String subTitle;
     private  String converImg;
    private  String m3u8_url;

    public String getM3u8_url() {
        return m3u8_url;
    }

    public void setM3u8_url(String m3u8_url) {
        this.m3u8_url = m3u8_url;
    }




    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getConverImg() {
        return converImg;
    }

    public void setConverImg(String converImg) {
        this.converImg = converImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "MediaItem{" +
                "name='" + name + '\'' +
                ", duration='" + duration + '\'' +
                ", size='" + size + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
