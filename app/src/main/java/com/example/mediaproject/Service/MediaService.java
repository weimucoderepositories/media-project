package com.example.mediaproject.Service;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;

import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.Nullable;


import com.example.mediaproject.Beans.MediaItem;
import com.example.mediaproject.IMyServicelInterface;

import java.io.IOException;
import java.util.ArrayList;

public class MediaService extends Service {

    private ArrayList<MediaItem> mediaItems = new ArrayList<>();
    private MediaPlayer mediaPlayer;
    IMyServicelInterface.Stub stub = new IMyServicelInterface.Stub() {

        MediaService service = MediaService.this;
        @Override
        public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {

        }

        @Override
        public void setPositon(int positon) throws RemoteException {
            service.setPositon(positon);
        }

        @Override
        public void start() throws RemoteException {
            service.start();
        }

        @Override
        public void pause() throws RemoteException {
            service.pause();
        }

        @Override
        public void next() throws RemoteException {
            service.next();
        }

        @Override
        public void previous() throws RemoteException {
            service.previous();

        }

        @Override
        public int getPosition() throws RemoteException {
            return service.getPosition();
        }

        @Override
        public int getCurrentTime() throws RemoteException {
            return  service.getCurrentTime();
        }

        @Override
        public int getTotalTime() throws RemoteException {
            return  service.getTotalTime();
        }

        @Override
        public String getSongName() throws RemoteException {
            return service.getSongName();
        }

        @Override
        public String getSongCreater() throws RemoteException {
              return service.getSongCreater();
        }

        @Override
        public void seekTime(int toTime) throws RemoteException {
            service.seekTime(toTime);
        }



    };
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return stub;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = new MediaPlayer();
        getDatas();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setPositon(int positon){
         if (mediaItems.size() > positon){

             try {
                 if (mediaPlayer!=null){

                     mediaPlayer.reset();
                     mediaPlayer.release();
                     mediaPlayer = null;
                 }
                 mediaPlayer = new MediaPlayer();
                 MediaItem mediaItem = mediaItems.get(positon);
                 mediaPlayer.setDataSource(mediaItem.getFilePath());
                 mediaPlayer.prepareAsync();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                    }
                });
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
    }
    private void start(){

    }

    private void pause(){

    }

    private void next(){

    }

    private void previous(){

    }

    private int getPosition(){
        return 0;
    }

    private int getCurrentTime(){
        return 0;
    }

    private int getTotalTime(){
        return 0;
    }

    private void seekTime(int toTime){

    }

    private String getSongName(){
        return "";
    }

    private String getSongCreater(){
        return "";
    }

    @SuppressLint("Range")
    private void getDatas(){
        new Thread(){

            @Override
            public void run() {
                super.run();


                    ContentResolver resolver = getContentResolver();
                    Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    String[] params = {
                            MediaStore.Audio.Media.DISPLAY_NAME, //在sd卡显示的名称
                            MediaStore.Audio.Media.DURATION,  //视频长度
                            MediaStore.Audio.Media.SIZE,  //视频大小
                            MediaStore.Audio.Media.DATA  //视频绝对路径
                    };
                    Cursor cursor = resolver.query(uri,params,null,null,null);
                    if (cursor != null){
                        while (cursor.moveToNext()){

                            String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
                            String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                            String size = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.SIZE));
                            String filePath = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                            MediaItem item = new MediaItem();
                            item.setName(name);
                            item.setDuration(duration);
                            item.setSize(size);
                            item.setFilePath(filePath);
                            mediaItems.add(item);
                        }
                    }

//                    ArrayList<MediaItem> temp = mediaItems;
//                    Log.d("TAG", "run: " + mediaItems.toString());
//                    handler.sendEmptyMessage(1000);

                }
        }.start();
    }
}
