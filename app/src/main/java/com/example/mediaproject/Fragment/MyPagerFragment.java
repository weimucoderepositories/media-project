package com.example.mediaproject.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mediaproject.Base.BasePager;

import java.util.List;

public class MyPagerFragment extends Fragment {

    private List<BasePager> pagerList;
    private int positon;
    public MyPagerFragment(List<BasePager> pagerList,int positon){
        this.pagerList = pagerList;
        this.positon = positon;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        BasePager basePager = getBasePager();
        if (basePager != null){
            return basePager.rootView;
        }
        return null;
    }

    private BasePager getBasePager() {
        BasePager basePager = pagerList.get(positon);
        if (basePager != null && !basePager.isInitView) {
            basePager.isInitView = true;
            basePager.initData();
        }
        return basePager;
    }
}
